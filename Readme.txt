﻿Для создания проекта использовал Visual Studio 2017 и .Net Framework 4.7.2
Файлы БД из папки db нужно развернуть в MS SQL Management Studio
В проекте нужно настроить подключение к БД в файлах Web.config и app.config проектов TrafficLight.Api, TrafficLight.Api.Tests и TrafficLight.Dal
Для проверки работоспособности сервиса нужно запустить TrafficLight.Api или опубликовать на сервере в IIS
Для запуска тестов нужно в Visual Studio нажать Ctrl+R,A