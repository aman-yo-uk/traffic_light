﻿using System.Web;
using System.Web.Http;
using TrafficLight.Api.Autofac;

namespace TrafficLight.Api {
	public class WebApiApplication : HttpApplication {
		protected void Application_Start() {
			GlobalConfiguration.Configure( WebApiConfig.Register );
			AutofacHelper.InitAutofac( GlobalConfiguration.Configuration );
			HttpConfiguration config = GlobalConfiguration.Configuration;
			config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
		}
	}
}