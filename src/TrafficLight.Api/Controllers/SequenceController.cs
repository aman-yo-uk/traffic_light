﻿using System;
using System.Web.Http;
using TrafficLight.Dto.Base;
using TrafficLight.Dto.Sequence;
using TrafficLight.Services;

namespace TrafficLight.Api.Controllers {
	[RoutePrefix( "sequence" )]
	public class SequenceController : ApiController {
		private readonly ISequenceService _sequenceService;

		public SequenceController(
			ISequenceService sequenceService
			) {
			_sequenceService = sequenceService;
		}

		/// <summary>
		/// Create and get new sequence
		/// </summary>
		/// <returns></returns>
		[Route( "create" )]
		public BaseResponse<SequenceDto> PostCreate() {
			try {
				var sequence = _sequenceService.GetSequence();
				return BaseResponse<SequenceDto>.GetSuccess( sequence );
			} catch (Exception ex) {
				return BaseResponse<SequenceDto>.GetError( "Failed to create a sequence" );
			}
		}
	}
}