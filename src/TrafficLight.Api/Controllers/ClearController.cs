﻿using System;
using System.Web.Http;
using TrafficLight.Dto.Base;
using TrafficLight.Services;

namespace TrafficLight.Api.Controllers {
	[RoutePrefix( "clear" )]
	public class ClearController : ApiController {
		private readonly IClearService _clearService;

		public ClearController(
			IClearService clearService
			) {
			_clearService = clearService;
		}

		/// <summary>
		/// Clear all data in service
		/// </summary>
		/// <returns></returns>
		[Route( "" )]
		public BaseResponse<string> GetClear() {
			try {
				_clearService.Clear();
				return BaseResponse<string>.GetSuccess( "ok" );
			} catch (Exception ex) {
				return BaseResponse<string>.GetError( "Could not clear service" );
			}
		}
	}
}