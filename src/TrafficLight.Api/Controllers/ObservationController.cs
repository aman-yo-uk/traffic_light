﻿using System;
using System.Web.Http;
using TrafficLight.Dto.Base;
using TrafficLight.Dto.Observation;
using TrafficLight.Services;

namespace TrafficLight.Api.Controllers {
	[RoutePrefix( "observation" )]
	public class ObservationController : ApiController {
		private readonly IObservationService _observationService;

		public ObservationController(
			IObservationService observationService
			) {
			_observationService = observationService;
		}

		/// <summary>
		/// Add observation
		/// </summary>
		/// <param name="observationData"></param>
		/// <returns></returns>
		[Route( "add" )]
		public BaseResponse<ObservationAddResponseDto> PostAdd([FromBody] ObservationAddRequestDto observationData) {
			try {
				var observationAddResponse = _observationService.Add( observationData );
				return BaseResponse<ObservationAddResponseDto>.GetSuccess( observationAddResponse );
			} catch (Exception ex) {
				return BaseResponse<ObservationAddResponseDto>.GetError( ex.Message );
			}
		}
	}
}