﻿using Autofac;
using Autofac.Integration.WebApi;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using TrafficLight.Dal;
using TrafficLight.Dal.Base;
using TrafficLight.Dal.Base.Impl;
using TrafficLight.Dal.Repositories;
using TrafficLight.Services;

namespace TrafficLight.Api.Autofac {
	public static class AutofacHelper {
		public static IContainer Container { get; private set; }

		public static void InitAutofac(HttpConfiguration config) {
			var builder = new ContainerBuilder();
			RegisterUow( builder );
			RegisterRepositories( builder );
			RegisterServices( builder );
			RegisterControllers( builder );

			// Set the dependency resolver to be Autofac
			Container = builder.Build();

			config.DependencyResolver = new AutofacWebApiDependencyResolver( Container );
		}

		private static void RegisterUow(ContainerBuilder builder) {
			builder.RegisterType<Uow<TrafficLightEfModel>>().As<IUow<TrafficLightEfModel>>().InstancePerRequest();
		}

		private static void RegisterRepositories(ContainerBuilder builder) {
			builder.RegisterAssemblyTypes( Assembly.GetAssembly( typeof( ISequenceRepository ) ) )
				.Where( _ => !_.IsInterface )
				.Where( _ => _.Name.EndsWith( "Repository" ) )
				.AsImplementedInterfaces()
				.InstancePerRequest();
		}

		private static void RegisterServices(ContainerBuilder builder) {
			builder.RegisterAssemblyTypes( Assembly.GetAssembly( typeof( ISequenceService ) ) )
				.Where( _ => !_.IsInterface )
				.Where( _ => _.Name.EndsWith( "Service" ) )
				.AsImplementedInterfaces()
				.InstancePerRequest();
		}

		private static void RegisterControllers(ContainerBuilder builder) {
			builder.RegisterApiControllers( Assembly.GetExecutingAssembly() );
		}
	}
}