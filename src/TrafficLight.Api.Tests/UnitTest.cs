﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using TrafficLight.Api.Controllers;
using TrafficLight.Dto.Observation;
using TrafficLight.Tools.Helpers;

namespace TrafficLight.Api.Tests {
	[TestClass]
	public class UnitTest : BaseTests {
		private string _sequence;

		[TestInitialize]
		public void TestStartObservation() {
			using (var scope = containerBuilder.BeginLifetimeScope()) {
				var clearController = scope.Resolve<ClearController>();
				var clearResult = clearController.GetClear();
				Assert.IsNotNull( clearResult );
				Assert.IsNotNull( clearResult.Response );
				Assert.AreEqual( "ok", clearResult.Response );

				var sequenceController = scope.Resolve<SequenceController>();
				var sequenceResult = sequenceController.PostCreate();
				Assert.IsNotNull( sequenceResult );
				Assert.IsNotNull( sequenceResult.Response );
				_sequence = sequenceResult.Response.Guid;
				Assert.IsNotNull( _sequence );
				Assert.AreNotEqual( "", _sequence );
			}
		}

		[TestMethod]
		public void TestYellowObservation() {
			using (var scope = containerBuilder.BeginLifetimeScope()) {
				var observationController = scope.Resolve<ObservationController>();
				var observationData = new ObservationAddRequestDto {
					Sequence = _sequence,
					Observation = new ObservationInfoDto {
						Color = "yellow"
					}
				};
				var observationResult = observationController.PostAdd( observationData );
				Assert.IsNotNull( observationResult );
				Assert.AreEqual( "error", observationResult.Status );
			}
		}

		[TestMethod]
		public void TestEmptyObservation() {
			using (var scope = containerBuilder.BeginLifetimeScope()) {
				var observationController = scope.Resolve<ObservationController>();
				var observationData = new ObservationAddRequestDto();
				var observationResult = observationController.PostAdd( observationData );
				Assert.IsNotNull( observationResult );
				Assert.AreEqual( "error", observationResult.Status );
			}
		}

		[TestMethod]
		public void TestNullObservation() {
			using (var scope = containerBuilder.BeginLifetimeScope()) {
				var observationController = scope.Resolve<ObservationController>();
				ObservationAddRequestDto observationData = null;
				var observationResult = observationController.PostAdd( observationData );
				Assert.IsNotNull( observationResult );
				Assert.AreEqual( "error", observationResult.Status );
			}
		}

		[TestMethod]
		public void TestObservation() {
			using (var scope = containerBuilder.BeginLifetimeScope()) {
				// Test first red
				var observationController = scope.Resolve<ObservationController>();
				var observationData = new ObservationAddRequestDto {
					Sequence = _sequence,
					Observation = new ObservationInfoDto {
						Color = "red"
					}
				};
				var observationResult = observationController.PostAdd( observationData );
				Assert.IsNotNull( observationResult );
				Assert.AreEqual( "error", observationResult.Status );

				// Generate auto test
				var random = new Random();
				var number = random.Next( 1, 10/*100*/ );
				var initNumber = number;

				// Missing in 2 section
				var missings = new Dictionary<int, List<int>>();
				for (int i = 0; i < 2; i++) {
					var sectionMissingCount = random.Next( 1, 8 );
					missings.Add( i, new List<int>() );

					for (int j = 0; j < sectionMissingCount; j++) {
						var position = random.Next( 0, 4/*7*/ );
						missings[i].Add( position );
					}
				}

				var numberMakets = NumberMaketsHelper.GetNumberMakets();

				while (number != 0) {
					var numbers = new List<string>();

					if (number < 10) {
						numbers.Add( ReplaceNumberLinesByMissingPositions( numberMakets[0], missings[0] ) );
						numbers.Add( ReplaceNumberLinesByMissingPositions( numberMakets[number], missings[1] ) );
					} else {
						var numbersBySections = number.ToString().Select( _ => int.Parse( _.ToString() ) ).ToList();
						for (int i = 0; i < numbersBySections.Count; i++) {
							numbers.Add( ReplaceNumberLinesByMissingPositions( numberMakets[numbersBySections[i]], missings[i] ) );
						}
					}

					observationData = new ObservationAddRequestDto {
						Sequence = _sequence,
						Observation = new ObservationInfoDto {
							Color = "green",
							Numbers = numbers
						}
					};
					observationResult = observationController.PostAdd( observationData );

					Assert.IsNotNull( observationResult );
					if (observationResult.Status == "ok")
						Assert.IsTrue( observationResult.Response.Start.Contains( initNumber ) );
					else
						break;

					if (observationResult.Response.Start.Count == 1) {
						foreach (var missing in missings)
						foreach (var position in missing.Value)
							Assert.IsTrue( observationResult.Response.Missing[missing.Key].Substring( position, 1 ) == "1" );
						break;
					}

					number--;
				}

				if (observationResult.Response.Start.Count != 1 && observationResult.Status == "ok") {
					// Observation first red
					observationData = new ObservationAddRequestDto {
						Sequence = _sequence,
						Observation = new ObservationInfoDto {
							Color = "red"
						}
					};
					observationResult = observationController.PostAdd( observationData );
					Assert.IsNotNull( observationResult );
					Assert.AreEqual( "ok", observationResult.Status );
					Assert.IsTrue( observationResult.Response.Start.Count == 1 );
					Assert.IsTrue( observationResult.Response.Start.Contains( initNumber ) );

					foreach (var missing in missings)
						foreach (var position in missing.Value)
							Assert.IsTrue( observationResult.Response.Missing[missing.Key].Substring( position, 1 ) == "1" );

					// Observation second red
					observationData = new ObservationAddRequestDto {
						Sequence = _sequence,
						Observation = new ObservationInfoDto {
							Color = "red"
						}
					};
					observationResult = observationController.PostAdd( observationData );
					Assert.IsNotNull( observationResult );
					Assert.AreEqual( "error", observationResult.Status );
				}
			}
		}

		private string ReplaceNumberLinesByMissingPositions(string numberMaket, List<int> positions) {
			for (int i = 0; i < positions.Count; i++) {
				var position = positions[i];
				if (numberMaket.Substring( position, 1 ) == "1") {
					numberMaket = numberMaket.Remove( position, 1 );
					numberMaket = numberMaket.Insert( position, "0" );
				}
			}

			return numberMaket;
		}

		[TestCleanup]
		public void TestEndObservation() {
			using (var scope = containerBuilder.BeginLifetimeScope()) {
				var clearController = scope.Resolve<ClearController>();
				var result = clearController.GetClear();
				Assert.IsNotNull( result );
				Assert.AreEqual( "ok", result.Response );
			}
		}
	}
}