﻿using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using System.Web.Http;
using TrafficLight.Api.Controllers;
using TrafficLight.Dal;
using TrafficLight.Dal.Base;
using TrafficLight.Dal.Base.Impl;
using TrafficLight.Dal.Repositories;
using TrafficLight.Services;

namespace TrafficLight.Api.Tests {
	[TestClass]
	public class BaseTests {
		protected IContainer containerBuilder;
		protected HttpConfiguration httpConfiguration;

		[TestInitialize]
		public void Init() {
			var builder = new ContainerBuilder();
			RegisterUow( builder );
			RegisterRepositories( builder );
			RegisterServices( builder );
			RegisterControllers( builder );

			// Set the dependency resolver to be Autofac
			containerBuilder = builder.Build();

			httpConfiguration = new HttpConfiguration {
				DependencyResolver = new AutofacWebApiDependencyResolver( containerBuilder )
			};

			GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver( containerBuilder );
		}

		private void RegisterUow(ContainerBuilder builder) {
			builder.RegisterType<Uow<TrafficLightEfModel>>().As<IUow<TrafficLightEfModel>>().InstancePerLifetimeScope();
		}

		private void RegisterRepositories(ContainerBuilder builder) {
			builder.RegisterAssemblyTypes( Assembly.GetAssembly( typeof( ISequenceRepository ) ) )
				.Where( _ => !_.IsInterface )
				.Where( _ => _.Name.EndsWith( "Repository" ) )
				.AsImplementedInterfaces()
				.InstancePerLifetimeScope();
		}

		private void RegisterServices(ContainerBuilder builder) {
			builder.RegisterAssemblyTypes( Assembly.GetAssembly( typeof( ISequenceService ) ) )
				.Where( _ => !_.IsInterface )
				.Where( _ => _.Name.EndsWith( "Service" ) )
				.AsImplementedInterfaces()
				.InstancePerLifetimeScope();
		}

		private void RegisterControllers(ContainerBuilder builder) {
			builder.RegisterApiControllers( Assembly.GetAssembly( typeof( SequenceController ) ) );
		}

		[TestCleanup]
		public void Cleanup() {
			httpConfiguration.Dispose();
			containerBuilder.Dispose();
		}
	}
}