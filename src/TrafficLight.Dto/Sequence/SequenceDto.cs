﻿using Newtonsoft.Json;
using TrafficLight.Dto.Base.Impl;

namespace TrafficLight.Dto.Sequence {
	public class SequenceDto : BaseDto {
		[JsonProperty( "sequence" )]
		public string Guid { get; set; }
	}
}