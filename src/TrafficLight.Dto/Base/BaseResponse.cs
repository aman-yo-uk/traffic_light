﻿using Newtonsoft.Json;

namespace TrafficLight.Dto.Base {
	/// <summary>
	/// Базовый ответ не типизированный (void)
	/// </summary>
	public class BaseResponse {
		/// <summary>
		/// Признак успешной обработки запроса
		/// </summary>
		[JsonProperty( "status" )]
		public string Status { get; protected set; }

		/// <summary>
		/// Сообщение ошибки
		/// </summary>
		[JsonProperty( "msg", DefaultValueHandling = DefaultValueHandling.Ignore )]
		public string Msg { get; protected set; }

		public static BaseResponse GetSuccess() {
			return new BaseResponse {
				Status = "ok"
			};
		}

		public static BaseResponse GetError(string errorMessage) {
			return new BaseResponse {
				Status = "error",
				Msg = errorMessage
			};
		}
	}
}