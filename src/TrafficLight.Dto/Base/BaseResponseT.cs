﻿using Newtonsoft.Json;
using System;

namespace TrafficLight.Dto.Base {
	/// <summary>
	/// Базовый ответ типизированный
	/// </summary>
	public class BaseResponse<T> : BaseResponse {
		/// <summary>
		/// Ответ метода сервиса
		/// </summary>
		[JsonProperty( "response", DefaultValueHandling = DefaultValueHandling.Ignore )]
		public T Response { get; set; }

		public static BaseResponse<T> GetSuccess(T result) {
			return new BaseResponse<T> {
				Status = "ok",
				Response = result,
				Msg = null
			};
		}

		public static BaseResponse<T> GetError(string errorMessage, T result = default( T )) {
			return new BaseResponse<T> {
				Status = "error",
				Response = result,
				Msg = errorMessage
			};
		}

		public static BaseResponse<T> GetError(Exception ex = null, T result = default( T )) {
			return new BaseResponse<T> {
				Status = "error",
				Response = result,
				Msg = ex.ToString()
			};
		}
	}
}