﻿using Newtonsoft.Json;

namespace TrafficLight.Dto.Base.Impl {
	public class BaseDto : IDto {
		[JsonIgnore]
		public long Id { get; set; }
		[JsonIgnore]
		public bool IsDeleted { get; set; }
	}
}