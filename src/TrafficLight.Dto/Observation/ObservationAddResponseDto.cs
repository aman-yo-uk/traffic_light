﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TrafficLight.Dto.Observation {
	public class ObservationAddResponseDto {
		[JsonProperty( "start" )]
		public List<int> Start { get; set; }

		[JsonProperty( "missing" )]
		public List<string> Missing { get; set; }
	}
}