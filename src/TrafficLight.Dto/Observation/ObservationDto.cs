﻿using System;
using TrafficLight.Dto.Base.Impl;

namespace TrafficLight.Dto.Observation {
	public class ObservationDto : BaseDto {
		public long SequenceId { get; set; }
		public string Color { get; set; }
		public string Numbers { get; set; }
		public DateTime BeginDateTime { get; set; }
		public DateTime EndDateTime { get; set; }
		public string Start { get; set; }
		public string Missing { get; set; }
	}
}