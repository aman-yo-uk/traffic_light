﻿using Newtonsoft.Json;
using TrafficLight.Dto.Sequence;

namespace TrafficLight.Dto.Observation {
	public class ObservationAddRequestDto {
		[JsonProperty( "observation" )]
		public ObservationInfoDto Observation { get; set; }

		[JsonProperty( "sequence" )]
		public string Sequence { get; set; }
	}
}