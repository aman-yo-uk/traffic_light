﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TrafficLight.Dto.Observation {
	public class ObservationInfoDto {
		[JsonProperty( "color" )]
		public string Color { get; set; }

		[JsonProperty( "numbers" )]
		public List<string> Numbers { get; set; }
	}
}