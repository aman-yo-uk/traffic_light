﻿namespace TrafficLight.Dto.Observation {
	public class ObservationProbabilityDto {
		public int Key { get; set; }
		public double MatchesCount { get; set; }
		public double NonMatchesCount { get; set; }
	}
}