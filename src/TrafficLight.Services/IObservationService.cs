﻿using TrafficLight.Dto.Observation;

namespace TrafficLight.Services {
	public interface IObservationService {
		ObservationAddResponseDto Add(ObservationAddRequestDto observationData);
	}
}