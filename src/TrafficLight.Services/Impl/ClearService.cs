﻿using System.Linq;
using TrafficLight.Dal;
using TrafficLight.Dal.Base;
using TrafficLight.Dal.Repositories;

namespace TrafficLight.Services.Impl {
	public class ClearService : IClearService {
		private readonly IUow<TrafficLightEfModel> _uow;
		private readonly ISequenceRepository _sequenceRepository;
		private readonly IObservationRepository _observationRepository;

		public ClearService(
			IUow<TrafficLightEfModel> uow,
			ISequenceRepository sequenceRepository,
			IObservationRepository observationRepository
			) {
			_uow = uow;
			_sequenceRepository = sequenceRepository;
			_observationRepository = observationRepository;
		}

		public bool Clear() {
			// TODO
			using (var transaction = _uow.BeginTransaction()) {
				var observations = _observationRepository.GetDtoQuery().ToList();
				foreach (var observation in observations)
					_observationRepository.DeleteDto( observation );

				var sequences = _sequenceRepository.GetDtoQuery().ToList();
				foreach (var sequence in sequences)
					_sequenceRepository.DeleteDto( sequence );

				transaction.Commit();
			}

			return true;
		}
	}
}