﻿using System;
using TrafficLight.Dal.Repositories;
using TrafficLight.Dto.Sequence;

namespace TrafficLight.Services.Impl {
	public class SequenceService : ISequenceService {
		private readonly ISequenceRepository _sequenceRepository;

		public SequenceService(
			ISequenceRepository sequenceRepository
			) {
			_sequenceRepository = sequenceRepository;
		}

		public SequenceDto GetSequence() {
			var sequence = new SequenceDto { Guid = Guid.NewGuid().ToString() };
			sequence.Id = _sequenceRepository.SaveDto( sequence );

			return sequence;
		}
	}
}