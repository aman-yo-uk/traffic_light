﻿using System;
using System.Collections.Generic;
using System.Linq;
using TrafficLight.Dal.Repositories;
using TrafficLight.Dto.Observation;
using TrafficLight.Tools.Extensions;
using TrafficLight.Tools.Helpers;

namespace TrafficLight.Services.Impl {
	public class ObservationService : IObservationService {
		private readonly IObservationRepository _observationRepository;
		private readonly ISequenceRepository _sequenceRepository;

		public ObservationService(
			IObservationRepository observationRepository,
			ISequenceRepository sequenceRepository
			) {
			_observationRepository = observationRepository;
			_sequenceRepository = sequenceRepository;
		}

		public ObservationAddResponseDto Add(ObservationAddRequestDto observationData) {
			var beginDateTime = DateTime.Now;

			if (observationData == null || observationData.Observation == null || observationData.Sequence.IsNullOrEmpty())
				throw new Exception( "No data for observation" );

			var color = observationData.Observation.Color;
			var sectionNumbers = observationData.Observation.Numbers;

			if (color != "red" && color != "green")
				throw new Exception( "Color must be 'red' or 'green'" );

			if (color == "red")
				sectionNumbers = new List<string> { "1110111", "1110111" };

			if (sectionNumbers.IsNullOrEmpty())
				throw new Exception( "Numbers is null or empty" );

			if (sectionNumbers.Count != 2)
				throw new Exception( "Numbers length must equal to 2" );

			if (sectionNumbers.Any( _ => _.Length != 7 ))
				throw new Exception( "Length of numbers must equal to 7" );

			if (sectionNumbers.Any( _ => _.Any( __ => __ != '0' && __ != '1' ) ))
				throw new Exception( "Numbers must contains only '0' and '1'" );

			var sequence = _sequenceRepository.GetDtoQuery()
				.FirstOrDefault( _ => _.Guid == observationData.Sequence );

			if (sequence == null)
				throw new Exception( "The sequence isn't found" );

			var observations = _observationRepository.GetDtoQuery()
				.Where( _ => _.SequenceId == sequence.Id )
				.ToList();

			if (observations.IsNullOrEmpty() && color == "red")
				throw new Exception( "There isn't enough data" );

			if (observations.Any( _ => _.Color == "red" ))
				throw new Exception( "The observation with color 'red' should be the last" );

			if (observations.Any( _ => _.Color == color && _.Numbers == JsonSerializerHelper.SerializeObject( sectionNumbers ) ))
				throw new Exception( "No solutions found" );

			// Список секций с возможно сломанными линиями
			var missings = new List<string>();

			// Макеты чисел
			var numberMakets = NumberMaketsHelper.GetNumberMakets();

			// Список чисел с которых возможно начался отсчет для первой и второй секция
			List<int> startSectionList1 = new List<int> { 0 };
			List<int> startSectionList2 = new List<int> { 0 };

			// Начинаем анализ если цвет зеленый
			if (color != "red") {
				// Анализируем каждую секцию
				for (int i = 0; i < sectionNumbers.Count; i++) {
					string sectionNumber = sectionNumbers[i];

					// Исходный список чисел с количеством совпадений и не совпадений
					var startProbabilityList = new List<ObservationProbabilityDto>();
					for (int j = 0; j < 10; j++)
						startProbabilityList.Add( new ObservationProbabilityDto { Key = j, MatchesCount = 0, NonMatchesCount = 0 } );

					// Анализируем каждую линию из 7 текущей секции
					for (int j = 0; j < sectionNumber.Length; j++) {
						var numberLine = sectionNumber[j].ToString();

						// Сравниваем линию с каждым макетом и делаем подсчет совпадений и не совпадений
						for (int k = 0; k < numberMakets.Count; k++) {
							if (numberLine == numberMakets[k].Substring( j, 1 ))
								startProbabilityList[k].MatchesCount++;
							else
								startProbabilityList[k].NonMatchesCount++;
						}
					}

					// Берем максимальные значения совпадений
					var startProbabilityMaxs = startProbabilityList.Select( _ => _.MatchesCount ).OrderByDescending( _ => _ ).Take( 2 ).ToList();

					// Выбираем наиболее вероятные числа с которых начался отсчет
					List<int> startSectionList = startProbabilityList
						//.Where( _ => _.MatchesCount > _.NonMatchesCount )
						.Where( _ => startProbabilityMaxs.Contains( _.MatchesCount ) )
						.Select( _ => _.Key ).ToList();

					// Присваиваем в список первой секции или второй, чтобы потом склеить
					if (i == 0)
						startSectionList1 = startSectionList;
					else
						startSectionList2 = startSectionList;

					// Исходный список номеров линий с количеством совпадений и не совпадений
					var missingProbabilityList = new List<ObservationProbabilityDto>();
					for (int j = 0; j < 7; j++)
						missingProbabilityList.Add( new ObservationProbabilityDto { Key = j, MatchesCount = 0, NonMatchesCount = 0 } );

					// Анализируем число текущей секции со списком возможных чисел для посчета совпадений и не совпадений для сломанных линий
					foreach (var startSection in startSectionList)
						CalcMissingProbability( sectionNumber, numberMakets[startSection], missingProbabilityList );

					// Берем максимальные значения совпадений
					var missingProbabilityMax = missingProbabilityList.Max( _ => _.NonMatchesCount );

					// Заполняем массив с наиболее вероятными сломанными линиями текущей секции
					missings.Add( string.Concat( missingProbabilityList
						.Select( _ => _.NonMatchesCount > _.MatchesCount && _.NonMatchesCount == missingProbabilityMax ? "1" : "0" ).ToList() ) );
				}
			}

			// Склеиваем каждое число первой секции с каждым числом второй секции
			var starts = new List<int>();
			for (int j = 0; j < startSectionList1.Count; j++)
				starts.AddRange( startSectionList2.Select( _ => startSectionList1[j].ToString() + _.ToString() ).Select( _ => int.Parse( _.ToString() ) ) );

			// Готовим ответ для заполнения результатами
			var res = new ObservationAddResponseDto {
				Start = new List<int>(),
				Missing = new List<string>()
			};

			// Если это не первое наблюдение, то анализируем с предыдущими результатами
			if (observations.HasValue()) {
				// Получаем количество наблюдений
				var lastObservationCount = observations.Count();

				// Получаем последнее наблюдение
				var lastObservation = observations.OrderByDescending( _ => _.Id ).FirstOrDefault();

				// Получаем результат возможных начальных чисел из последнего наблюдения
				var lastStarts = JsonSerializerHelper.DeserializeObject<List<int>>( lastObservation.Start );

				// Увеличиваем каждое число текущего наблюдения на количество прошлых наблюдений
				for (int i = 0; i < starts.Count; i++)
					starts[i] += lastObservationCount;

				// Добавляем в результат только числа которые есть в обоих наблюдениях
				res.Start.AddRange( starts.Intersect( lastStarts ) );

				// Получаем результат возможных сломанных линий из последнего наблюдения
				var lastObservationMissings = JsonSerializerHelper.DeserializeObject<List<string>>( lastObservation.Missing );

				// Если цвет зеленый или начальных чисел больше 1 то сравниваем результаты текущего наблюдения с последним
				if (color != "red" && res.Start.Count > 1) {
					for (int i = 0; i < missings.Count; i++) {
						var missing = "";
						var missingList = missings[i].ToList();
						var lastObservationMissingList = lastObservationMissings[i].ToList();
						for (int j = 0; j < missingList.Count; j++) {
							missing += missingList[j] == lastObservationMissingList[j] ? missingList[j] : missingList[j] == '1' ? missingList[j] : lastObservationMissingList[j];
						}
						res.Missing.Add( missing );
					}
				} else {
					// Если цвет красный или начальное число равно 1 то анализируем самое первое наблюдение
					var firstObservation = observations.FirstOrDefault();

					// Получаем числа переданные на первое наблюдение
					var firstObservationNumbers = JsonSerializerHelper.DeserializeObject<string[]>( firstObservation.Numbers );

					foreach (var startSection in res.Start) {
						// Формируем список чисел каждой секции по отдельности из текущего результата
						List<int> resStarts = new List<int>();
						if (startSection < 10) {
							resStarts.Add( 0 );
							resStarts.Add( startSection );
						} else {
							resStarts.AddRange( startSection.ToString().Select( _ => int.Parse( _.ToString() ) ).ToList() );
						}

						// Перебираем числа переданные на первое наблюдение
						for (int i = 0; i < firstObservationNumbers.Length; i++) {
							// Получаем число из i-ой секции
							var resStart = resStarts[i];

							// i-ое число в линиях из первого наблюдения
							string number = firstObservationNumbers[i];

							// Объявляем исходный список номеров линий с количеством совпадений и не совпадений и делаем подсчет
							var missingProbabilityList = new List<ObservationProbabilityDto>();
							for (int j = 0; j < 7; j++)
								missingProbabilityList.Add( new ObservationProbabilityDto { Key = j, MatchesCount = 0, NonMatchesCount = 0 } );

							// Анализируем число секции первого наблюдения со списком возможных чисел для посчета совпадений и не совпадений для сломанных линий
							CalcMissingProbability( number, numberMakets[resStart], missingProbabilityList, false );

							var missingProbabilityMax = missingProbabilityList.Max( _ => _.NonMatchesCount );

							// Заполняем массив с наиболее вероятными сломанными линиями текущей секции
							missings.Add( string.Concat( missingProbabilityList
								.Select( _ => _.NonMatchesCount > _.MatchesCount && _.NonMatchesCount == missingProbabilityMax ? "1" : "0" ).ToList() ) );
						}
					}

					res.Missing = missings;
				}
			} else {
				// Если это первое наблюдение то просто присваиваем результаты
				res.Start = starts;
				res.Missing = missings;
			}

			if (res.Start.IsNullOrEmpty())
				throw new Exception( "No solutions found" );

			// Сохраняем наблюдение в БД
			var observation = new ObservationDto {
				BeginDateTime = beginDateTime,
				Color = color,
				EndDateTime = DateTime.Now,
				SequenceId = sequence.Id,
				Numbers = JsonSerializerHelper.SerializeObject( sectionNumbers ),
				Start = JsonSerializerHelper.SerializeObject( res.Start ),
				Missing = JsonSerializerHelper.SerializeObject( res.Missing ),
			};
			_observationRepository.SaveDto( observation );

			return res;
		}

		/// <summary>
		/// Метод для анализа числа секции со списком возможных чисел для посчета совпадений и не совпадений для сломанных линий
		/// </summary>
		/// <param name="number">Число в линиях (пример 1110111)</param>
		/// <param name="numberN">Номер числа из макетов</param>
		/// <param name="missingProbabilityList">Список номеров линий с количеством совпадений и не совпадений и делаем подсчет</param>
		/// <param name="untruly">Для уточнения подсчета, если не уверены на первых этапах наблюдений</param>
		/// <returns></returns>
		private void CalcMissingProbability(string number, string numberN, List<ObservationProbabilityDto> missingProbabilityList, bool untruly = true) {
			for (int j = 0; j < number.Length; j++) {
				var digit = number[j].ToString();
				if (digit == "0") {
					if (digit != numberN.Substring( j, 1 )) {
						missingProbabilityList[j].NonMatchesCount++;
					} else if (untruly) {
						missingProbabilityList[j].NonMatchesCount+=0.5;
					}
				} else {
					missingProbabilityList[j].MatchesCount++;
				}
			}
		}
	}
}