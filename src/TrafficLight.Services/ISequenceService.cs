﻿using TrafficLight.Dto.Sequence;

namespace TrafficLight.Services {
	public interface ISequenceService {
		SequenceDto GetSequence();
	}
}