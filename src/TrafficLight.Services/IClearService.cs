﻿namespace TrafficLight.Services {
	public interface IClearService {
		bool Clear();
	}
}