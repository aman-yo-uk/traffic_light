﻿using System;
using System.Data.Entity;
using System.Linq;

namespace TrafficLight.Dal.Base {
	public class BaseDbContext : DbContext {
		public BaseDbContext(string efModel) {

		}

		public override int SaveChanges() {
			var changedEntities = ChangeTracker.Entries().Where( e => e.State == EntityState.Added || e.State == EntityState.Modified ).ToList();

			changedEntities.ForEach( _ => {
				var createdDateProperty = _.Entity.GetType().GetProperty( "CreatedDate" );
				if (createdDateProperty != null) {
					if (_.State == EntityState.Added)
						createdDateProperty.SetValue( _.Entity, DateTime.Now );
					else
						createdDateProperty.SetValue( _.Entity, _.OriginalValues.GetValue<DateTime>( createdDateProperty.Name ) );
				}

				var modifiedDateProperty = _.Entity.GetType().GetProperty( "ModifiedDate" );
				if (modifiedDateProperty != null) {
					modifiedDateProperty.SetValue( _.Entity, DateTime.Now );
				}
			} );

			return base.SaveChanges();
		}
	}
}