﻿using System.Data.Entity;

namespace TrafficLight.Dal.Base.Impl {
	public class Uow<TDbContext> : IUow<TDbContext>
		where TDbContext : DbContext, new() {

		public TDbContext Db { get; private set; }

		public TDbContext GetDbContext() {
			return Db ?? ( Db = new TDbContext() );
		}

		public DbContextTransaction BeginTransaction() {
			return GetDbContext().Database.BeginTransaction();
		}
	}
}