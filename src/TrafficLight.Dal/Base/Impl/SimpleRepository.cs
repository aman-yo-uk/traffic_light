﻿using TrafficLight.Dto.Base;
using TrafficLight.Tools.Extensions;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace TrafficLight.Dal.Base.Impl {
	public abstract class SimpleRepository<TDbContext, TDomainModel, TDto> : ISimpleRepository<TDomainModel, TDto>
		where TDomainModel : class, IDomainModel
		where TDto : class, IDto
		where TDbContext : DbContext {

		private readonly IUow<TDbContext> _uow;

		public SimpleRepository(IUow<TDbContext> uow) {
			_uow = uow;
		}

		public IQueryable<TDto> GetDtoQuery(bool getDeleted = false) {
			return _uow.GetDbContext().Set<TDomainModel>()
				.WhereIf( !getDeleted, _ => !_.IsDeleted )
				.Select( ToDto() );
		}

		public TDto GetDtoById(long id) {
			var dto = GetDtoQuery( true )
				.FirstOrDefault( x => x.Id == id );

			return dto;
		}

		public long SaveDto(TDto dto) {
			var source = _uow.GetDbContext().Set<TDomainModel>().FirstOrDefault( _ => _.Id == dto.Id );
			var domainModel = ToDomainModel()( dto, source );

			if (dto.Id == 0)
				_uow.GetDbContext().Set<TDomainModel>().Add( domainModel );
			else
				_uow.GetDbContext().Entry( source ).CurrentValues.SetValues( domainModel );

			_uow.GetDbContext().SaveChanges();
			return domainModel.Id;
		}

		public bool DeleteDto(TDto dto) {
			var source = _uow.GetDbContext().Set<TDomainModel>().FirstOrDefault( _ => _.Id == dto.Id && _.IsDeleted == false );
			if (source != null) {
				source.IsDeleted = true;
				_uow.GetDbContext().SaveChanges();
				return true;
			}
			return false;
		}

		public abstract Expression<Func<TDomainModel, TDto>> ToDto();

		public abstract Func<TDto, TDomainModel, TDomainModel> ToDomainModel();
	}
}