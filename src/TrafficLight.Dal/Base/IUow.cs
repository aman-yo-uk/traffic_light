﻿using System.Data.Entity;

namespace TrafficLight.Dal.Base {
	public interface IUow<TDbContext>
		where TDbContext : DbContext {
		TDbContext GetDbContext();
		DbContextTransaction BeginTransaction();
	}
}