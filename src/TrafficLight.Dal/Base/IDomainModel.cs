﻿namespace TrafficLight.Dal.Base {
	public interface IDomainModel {
		long Id { get; set; }
		bool IsDeleted { get; set; }
	}
}