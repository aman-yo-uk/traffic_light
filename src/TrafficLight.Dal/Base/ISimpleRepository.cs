﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace TrafficLight.Dal.Base {
	public interface ISimpleRepository<TDomainModel, TDto> {
		IQueryable<TDto> GetDtoQuery( bool getDeleted = false );
		TDto GetDtoById( long id );
		long SaveDto( TDto dto );
		bool DeleteDto( TDto dto );
		Expression<Func<TDomainModel, TDto>> ToDto();
		Func<TDto, TDomainModel, TDomainModel> ToDomainModel();
	}
}