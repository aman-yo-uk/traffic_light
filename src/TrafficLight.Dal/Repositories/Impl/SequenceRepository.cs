﻿using System;
using System.Linq.Expressions;
using TrafficLight.Dal.Base;
using TrafficLight.Dal.Base.Impl;
using TrafficLight.Dto.Sequence;

namespace TrafficLight.Dal.Repositories.Impl {
	public class SequenceRepository : SimpleRepository<TrafficLightEfModel, Sequence, SequenceDto>, ISequenceRepository {
		private readonly IUow<TrafficLightEfModel> _uow;

		public SequenceRepository(IUow<TrafficLightEfModel> uow) : base( uow ) {
			_uow = uow;
		}

		public override Func<SequenceDto, Sequence, Sequence> ToDomainModel() {
			return (_, source) => new Sequence {
				Id = source != null ? source.Id : _.Id,
				Guid = _.Guid
			};
		}

		public override Expression<Func<Sequence, SequenceDto>> ToDto() {
			return _ => new SequenceDto {
				Id = _.Id,
				Guid = _.Guid
			};
		}
	}
}