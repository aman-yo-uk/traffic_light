﻿using System;
using System.Linq.Expressions;
using TrafficLight.Dal.Base;
using TrafficLight.Dal.Base.Impl;
using TrafficLight.Dto.Observation;

namespace TrafficLight.Dal.Repositories.Impl {
	public class ObservationRepository : SimpleRepository<TrafficLightEfModel, Observation, ObservationDto>, IObservationRepository {
		private readonly IUow<TrafficLightEfModel> _uow;

		public ObservationRepository(IUow<TrafficLightEfModel> uow) : base( uow ) {
			_uow = uow;
		}

		public override Func<ObservationDto, Observation, Observation> ToDomainModel() {
			return (_, source) => new Observation {
				Id = source != null ? source.Id : _.Id,
				SequenceId = _.SequenceId,
				Color = _.Color,
				Numbers = _.Numbers,
				BeginDateTime = _.BeginDateTime,
				EndDateTime = _.EndDateTime,
				Start = _.Start,
				Missing = _.Missing
			};
		}

		public override Expression<Func<Observation, ObservationDto>> ToDto() {
			return _ => new ObservationDto {
				Id = _.Id,
				SequenceId = _.SequenceId,
				Color = _.Color,
				Numbers = _.Numbers,
				BeginDateTime = _.BeginDateTime,
				EndDateTime = _.EndDateTime,
				Start = _.Start,
				Missing = _.Missing
			};
		}
	}
}