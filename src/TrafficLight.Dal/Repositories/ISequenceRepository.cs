﻿using TrafficLight.Dal.Base;
using TrafficLight.Dto.Sequence;

namespace TrafficLight.Dal.Repositories {
	public interface ISequenceRepository : ISimpleRepository<Sequence, SequenceDto> {

	}
}