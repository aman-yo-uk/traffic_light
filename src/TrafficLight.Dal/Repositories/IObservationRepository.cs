﻿using TrafficLight.Dal.Base;
using TrafficLight.Dto.Observation;

namespace TrafficLight.Dal.Repositories {
	public interface IObservationRepository : ISimpleRepository<Observation, ObservationDto> {

	}
}