﻿using System.Collections.Generic;

namespace TrafficLight.Tools.Helpers {
	public static class NumberMaketsHelper {
		public static Dictionary<int, string> GetNumberMakets() {
			return new Dictionary<int, string> {
				{ 0, "1110111" },
				{ 1, "0010010" },
				{ 2, "1011101" },
				{ 3, "1011011" },
				{ 4, "0111010" },
				{ 5, "1101011" },
				{ 6, "1101111" },
				{ 7, "1010010" },
				{ 8, "1111111" },
				{ 9, "1111011" }
			};
		}
	}
}