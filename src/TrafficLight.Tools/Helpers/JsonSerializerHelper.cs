﻿using Newtonsoft.Json;

namespace TrafficLight.Tools.Helpers {
	public static class JsonSerializerHelper {
		/// <summary>
		/// Сериализует объекты в json используя принятые настройки
		/// </summary>
		public static string SerializeObject(object @object) {
			var jsonParams = new JsonSerializerSettings { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat };
			var json = JsonConvert.SerializeObject( @object, jsonParams );
			return json;
		}

		/// <summary>
		/// Сериализует объекты в json используя принятые настройки
		/// </summary>
		public static T DeserializeObject<T>(string json) {
			var jsonParams = new JsonSerializerSettings { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat };
			var objectT = JsonConvert.DeserializeObject<T>( json, jsonParams );
			return objectT;
		}
	}
}